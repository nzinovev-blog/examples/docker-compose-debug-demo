package blog.nzinovev.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FancyController {

    @GetMapping("/{name}")
    public String getName(@PathVariable String name) {
        return name;
    }
}
