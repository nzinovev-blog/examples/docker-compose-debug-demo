package blog.nzinovev.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerComposeDebugDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerComposeDebugDemoApplication.class, args);
	}

}
