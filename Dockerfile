FROM openjdk:16-alpine3.13
ARG JAR_FILE=/build/libs/*.jar
COPY ${JAR_FILE} demo-app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar","/demo-app.jar"]